<%--
  Created by IntelliJ IDEA.
  User: costa
  Date: 05/07/2018
  Time: 18:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Navigation -->
<nav class="navbar navbar-expand-sm navbar-dark">
    <!-- Brand -->
    <a class="navbar-brand" href="<c:url value="/" />">TP Fil Rouge Java EE</a>

    <!-- Links -->
    <ul class="navbar-nav">
        <!--<li class="nav-item">
            <a class="nav-link" href="#">Link 1</a>
        </li>-->

        <!-- Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Client</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<c:url value="/CreationClient" />">Nouveau Client</a>
                <a class="dropdown-item" href="<c:url value="/ListeClients" />">Liste des clients</a>
            </div>
        </li>
        <!-- Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Commande</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<c:url value="/CreationCommande" />">Nouvelle Commande</a>
                <a class="dropdown-item" href="<c:url value="/ListeCommandes" />">Liste des commandes</a>
            </div>
        </li>
    </ul>
</nav>
