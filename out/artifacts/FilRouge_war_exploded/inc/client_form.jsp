<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: costa
  Date: 13/07/2018
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


        <label for="nomClient">Nom <span class="requis">*</span></label>
        <input type="text" id="nomClient" name="nomClient" value="<c:out value='${newClient.nom}'/>" size="20" maxlength="20" />
        <span class="erreur">${form.erreurs['nomClient']}</span>
        <br />

        <label for="prenomClient">Prénom </label>
        <input type="text" id="prenomClient" name="prenomClient" value="<c:out value='${newClient.prenom}'/>" size="20" maxlength="20" />
        <span class="erreur">${form.erreurs['prenomClient']}</span>
        <br />

        <label for="adresseClient">Adresse de livraison <span class="requis">*</span></label>
        <input type="text" id="adresseClient" name="adresseClient" value="<c:out value='${newClient.adresse}'/>" size="20" maxlength="50" />
        <span class="erreur">${form.erreurs['adresseClient']}</span>
        <br />

        <label for="telephoneClient">Numéro de téléphone <span class="requis">*</span></label>
        <input type="text" id="telephoneClient" name="telephoneClient" value="<c:out value='${newClient.tel}'/>" size="20" maxlength="20" />
        <span class="erreur">${form.erreurs['telephoneClient']}</span>
        <br />

        <label for="emailClient">Adresse email</label>
        <input type="email" id="emailClient" name="emailClient" value="<c:out value='${newClient.email}'/>" size="20" maxlength="60" style="margin-bottom: 1%;"/>
        <span class="erreur">${form.erreurs['emailClient']}</span>
        <br />

        <label for="image">Image</label>
        <input type="file" id="image" name="image" value="<c:out value="${image.nom}"/>" />
        <span class="erreur">${form.erreurs['image']}</span>
        <br />

