$(document).ready(function() {
    $('input[type=radio]').on("click", function()
    {
        var newClient = $('input[type=radio][name=choixNouveauClient]:checked').attr('value');
        if(newClient === "ancienClient")
        {
            $('.clientEx').show();
            $('.clientNonEx').hide();
        }
        if(newClient === "nouveauClient")
        {
            $('.clientEx').hide();
            $('.clientNonEx').show();
        }
    })
});