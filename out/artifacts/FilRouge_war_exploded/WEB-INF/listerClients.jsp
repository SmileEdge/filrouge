<!DOCTYPE html>
<html lang="fr">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="../inc/head.jsp" />
<body>
<c:import url="../inc/header.jsp" />
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
            <c:choose>
                <c:when test="${!empty listeClients}">
                        <table class="liste">
                            <tr>
                                <td>Nom</td>
                                <td>Prénom</td>
                                <td>Adresse</td>
                                <td>Téléphone</td>
                                <td>E-mail</td>
                                <td>Image</td>
                                <td class="action">Action</td>
                            </tr>
                            <c:forEach var="listeClients" items="${ listeClients }" varStatus="boucle">
                            <tr class="${boucle.index % 2 == 0 ? 'pair' : 'impair'}">
                                <td><c:out value="${listeClients.nom}"/></td>
                                <td><c:out value="${listeClients.prenom}"/></td>
                                <td><c:out value="${listeClients.adresse}"/></td>
                                <td><c:out value="${listeClients.tel}"/></td>
                                <td><c:out value="${listeClients.email}"/></td>
                                <td>
                                <c:choose>
                                    <c:when test="${!empty listeClients.image}">
                                        <a href="<c:url value="images/${listeClients.image}"/>">Voir</a>
                                    </c:when>
                                    <c:otherwise>
                                        Pas d'image
                                    </c:otherwise>
                                </c:choose>
                                </td>
                                <td><a href="<c:url value="/SuppressionClient"><c:param name="id" value="${listeClients.id}" /></c:url>"><img src="<c:url value="./inc/supprimer.png" />"></a></td>
                            </tr>
                            </c:forEach>
                        </table>
                </c:when>
                <c:otherwise>
                    <p class="erreur">Aucun client enregistré</p>
                </c:otherwise>
            </c:choose>
        </div>
      </div>
    </div>
<c:import url="../inc/footer.jsp" />
  </body>

</html>
