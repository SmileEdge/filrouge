package com.couderc3u.filrouge.filters;

import com.couderc3u.filrouge.dao.CommandeDAO;
import com.couderc3u.filrouge.entities.Client;
import com.couderc3u.filrouge.entities.Commande;
import com.couderc3u.filrouge.dao.ClientDAO;

import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebFilter( urlPatterns = "/*")
public class ListeSession implements Filter {

    private static final String ATT_LISTE_CLIENT = "listeClients";
    private static final String ATT_LISTE_COMMANDE = "listeCommandes";

    @EJB
    private CommandeDAO commandeDAO;
    @EJB
    private ClientDAO clientDAO;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)servletRequest;
        HttpSession session   = req.getSession();
        verifClientSession(session);
        verifCommandeSession(session);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void verifClientSession(HttpSession session)
    {

        List<Client> listeClients = (List<Client>)session.getAttribute(ATT_LISTE_CLIENT);

        if(listeClients == null)
        {
            listeClients = clientDAO.lister();
            session.setAttribute(ATT_LISTE_CLIENT, listeClients);
        }
    }

    private void verifCommandeSession(HttpSession session)
    {
        List<Commande> listeCommandes = (List<Commande>)session.getAttribute(ATT_LISTE_COMMANDE);

        if(listeCommandes == null)
        {
            listeCommandes = commandeDAO.lister();
            session.setAttribute(ATT_LISTE_COMMANDE, listeCommandes);
        }
    }
}
