package com.couderc3u.filrouge.entities;

import com.couderc3u.filrouge.tools.JodaDateTimeConverter;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

@Entity
public class Commande implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @ManyToOne
    @JoinColumn( name = "id_client")
    private Client client;
    @Column( name = "mode_paiement")
    private String paiement;
    @Column( name = "statut_paiement")
    private String statutP;
    @Column( name = "mode_livraison")
    private String livraison;
    @Column( name = "statut_livraison")
    private String statutL;
    private double montant;

    @Column( columnDefinition = "TIMESTAMP" )
    @Converter(name = "dateTimeConverter", converterClass = JodaDateTimeConverter.class)
    @Convert( "dateTimeConverter" )
    private DateTime date;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getPaiement() {
        return paiement;
    }

    public void setPaiement(String paiement) {
        if(paiement != null)
            this.paiement = new String(paiement.getBytes(), StandardCharsets.UTF_8);
    }

    public String getStatutP() {
        return statutP;
    }

    public void setStatutP(String statutP) {
        if(statutP != null)
            this.statutP = new String(statutP.getBytes(), StandardCharsets.UTF_8);
    }

    public String getLivraison() {
        return livraison;
    }

    public void setLivraison(String livraison) {
        if(livraison != null)
            this.livraison = new String(livraison.getBytes(), StandardCharsets.UTF_8);
    }

    public String getStatutL() {
        return statutL;
    }

    public void setStatutL(String statutL) {
        if(statutL != null)
            this.statutL = new String(statutL.getBytes(), StandardCharsets.UTF_8);
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
