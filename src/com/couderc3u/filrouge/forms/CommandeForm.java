package com.couderc3u.filrouge.forms;

import com.couderc3u.filrouge.Exceptions.FormValidationException;
import com.couderc3u.filrouge.dao.CommandeDAO;
import com.couderc3u.filrouge.entities.Commande;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;

public class CommandeForm
{
    private boolean ok;

    private static final String _nom             = "nomClient";
    private static final String _prenom          = "prenomClient";
    private static final String _adresse         = "adresseClient";
    private static final String _tel             = "telephoneClient";
    private static final String _email           = "emailClient";
    private static final String _newCommande     = "newCommande";
    private static final String _montant         = "montantCommande";
    private static final String _modePaiement    = "modePaiementCommande";
    private static final String _statutPaiement  = "statutPaiementCommande";
    private static final String _modeLivraison   = "modeLivraisonCommande";
    private static final String _statutLivraison = "statutLivraisonCommande";
    private static final String _dateformat      = "dd/MM/yyyy HH:mm:ss";
    private static final String erreur          = "<p class='erreur'>Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creerCommande.jsp\">Cliquez ici</a> pour accéder au formulaire de création d'un client.</p>";
    private static final String succes          = "<p class='succes'>Commande créé avec succès !</p>";
    private static final String forward         = "/afficherCommande.jsp";
    private static final String message         = "message";
    
    private String              resultat;
    private Map<String, String> erreurs = new HashMap<String, String>();
    private CommandeDAO commandeDAO;

    public CommandeForm(CommandeDAO commandeDAO) {
        this.commandeDAO = commandeDAO;
    }

    public String getResultat()
    {
        return resultat;
    }

    public Map<String, String> getErreurs()
    {
        return erreurs;
    }

    private void setErreur(String champ, String message)
    {
        erreurs.put(champ, message);
    }

    public boolean OK()
    {
        return ok;
    }
    
    private static String getValeurChamp(HttpServletRequest request, String nomChamp)
    {
        String valeur = request.getParameter(nomChamp);
        if (valeur == null || valeur.trim().length() == 0)
        {
            return null;
        }
        else
        {
            return valeur.trim();
        }
    }

    public Commande creationCommande(HttpServletRequest request)
    {
        Commande newCommande = new Commande();

        DateTime date = new DateTime();

        String montant    = getValeurChamp(request, _montant);
        String modePaiement    = getValeurChamp(request, _modePaiement);
        String statutPaiement  = getValeurChamp(request, _statutPaiement);
        String modeLivraison   = getValeurChamp(request, _modeLivraison);
        String statutLivraison = getValeurChamp(request, _statutLivraison);

        newCommande.setDate(date);

        try
        {
            newCommande.setMontant(validationMontant(montant));
        } catch (Exception e)
        {
            setErreur(_montant, e.getMessage());
        }

        try
        {
            ValidationObl(modePaiement);
        }
        catch (Exception e)
        {
            setErreur(_modePaiement, e.getMessage());
        }
        newCommande.setPaiement(modePaiement);

        try
        {
            Validation(statutPaiement);
        }
        catch (Exception e)
        {
            setErreur(_statutPaiement, e.getMessage());
        }
        newCommande.setStatutP(statutPaiement);

        try
        {
            ValidationObl(modeLivraison);
        }
        catch (Exception e)
        {
            setErreur(_modeLivraison, e.getMessage());
        }
        newCommande.setLivraison(modeLivraison);

        try
        {
            Validation(statutLivraison);
        }
        catch (Exception e)
        {
            setErreur(_statutLivraison, e.getMessage());
        }
        newCommande.setStatutL(statutLivraison);

        if (erreurs.isEmpty())
        {
            resultat = "Commande créée avec succès";
            ok = true;
        }
        else
        {
            resultat = "Échec de la création de la commande.";
            ok = false;
        }

        return newCommande;
    }

    private double validationMontant(String montant) throws Exception
    {
        double montantN;
        try
        {
            if(montant == null)
                throw new FormValidationException("Merci de saisir un montant.");

            montantN = Double.parseDouble(montant);
            if(montantN < 0)
            {
                throw new FormValidationException("Merci de saisir un montant valide.");
            }
        } catch (NumberFormatException e)
        {
            throw new FormValidationException("Merci de saisir un montant valide.");
        }
        return montantN;
    }

    private void Validation(String aVal) throws Exception
    {
        if (aVal != null && aVal.length() < 2)
        {
            throw new FormValidationException("Doit contenir au moins 2 caractères.");
        }
    }

    private void ValidationObl(String aVal) throws Exception
    {
        if (aVal != null)
        {
            if(aVal.length() < 2)
            {
                throw new FormValidationException("Doit contenir au moins 2 caractères.");
            }
        }
        else
            throw new FormValidationException("Merci de saisir cette information.");
    }
}
