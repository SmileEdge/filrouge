package com.couderc3u.filrouge.dao;

import com.couderc3u.filrouge.Exceptions.DAOException;
import com.couderc3u.filrouge.entities.Client;
import com.couderc3u.filrouge.entities.Commande;
import org.joda.time.DateTime;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.sql.*;
import java.util.HashMap;
import java.util.Date;
import java.util.List;

@Stateless
public class CommandeDAO{
    private static final String JPQL_SELECT        = "SELECT c FROM Commande c ORDER BY c.id";
    private static final String JPQL_SELECT_PAR_ID = "SELECT c FROM Commande c WHERE c.id =:id";
    private static final String PARAM_ID           = "id";
    private static final String JPQL_DELETE_PAR_ID = "DELETE FROM Commande c WHERE c.id =:id ";

    @PersistenceContext( unitName = "javaee_PU" )
    private EntityManager em;

    public Commande trouver( long id ) throws DAOException {
        try {
            return em.find( Commande.class, id );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    public void creer( Commande commande ) throws DAOException {
        try {
            em.persist( commande );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    public List<Commande> lister() throws DAOException {
        try {
            TypedQuery<Commande> query = em.createQuery( "SELECT c FROM Commande c ORDER BY c.id", Commande.class );
            return query.getResultList();
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    public void supprimer( Commande commande ) throws DAOException {
        try {
            em.remove( em.merge( commande ) );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }
}