package com.couderc3u.filrouge.servlets;

import com.couderc3u.filrouge.dao.CommandeDAO;
import com.couderc3u.filrouge.entities.Client;
import com.couderc3u.filrouge.entities.Commande;
import com.couderc3u.filrouge.dao.ClientDAO;
import com.couderc3u.filrouge.forms.ClientForm;
import com.couderc3u.filrouge.forms.CommandeForm;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@WebServlet( urlPatterns = { "/CreationCommande" }, initParams = @WebInitParam( name = "chemin", value = "/fichiers/images/" ))
@MultipartConfig( location = "/fichiers", maxFileSize = 10 * 1024 * 1024, maxRequestSize = 5 * 10 * 1024 * 1024, fileSizeThreshold = 1024 * 1024 )
public class CreationCommande extends HttpServlet
{
    private static final String ATT_FORM = "form";
    private static final String ATT_FORMC = "formCommande";
    private static final String VUE = "/WEB-INF/listerCommandes.jsp";
    private static final String FORM = "/WEB-INF/creerCommande.jsp";
    private static final String ATT_LISTE = "listeCommandes";
    private static final String ATT_LISTE_CLIENT = "listeClients";

    private static final String CHAMP_IMAGE     = "image";
    public static final String CHEMIN      = "chemin";
    public static final String ATT_IMAGE = "image";

    @EJB
    private CommandeDAO commandeDAO;
    @EJB
    private ClientDAO clientDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        this.getServletContext().getRequestDispatcher( FORM ).forward( req, resp );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        HttpSession session   = req.getSession();
        Client      newClient = null;
        ClientForm  form = null;
        CommandeForm formCommande = new CommandeForm(commandeDAO);
        Commande newCommande = formCommande.creationCommande(req);

        String choixNouveauClient = req.getParameter("choixNouveauClient");
        if ( "nouveauClient".equals( choixNouveauClient ) )
        {
            form = new ClientForm(clientDAO);
            String chemin = this.getServletConfig().getInitParameter( CHEMIN );
            newClient = form.creationClient(req, chemin);
            req.setAttribute(ATT_FORM, form);
        }
        else if ( "ancienClient".equals( choixNouveauClient ) )
        {
            Long id = Long.parseLong(req.getParameter("ClientSession"));
            List<Client> listeClients = (List<Client>)session.getAttribute(ATT_LISTE);
            for (Client c : listeClients) {
                if (c.getId().equals(id)) {
                    newClient = c;
                }
            }
        }
        else
        {
            throw new ServletException("Erreur Client Commande, Choix client :" + req.getParameter("choixNouveauClient"));
        }
        newCommande.setClient(newClient);

        req.setAttribute(ATT_FORMC, formCommande);

        if(formCommande.OK() && (form == null || form.getErreurs().isEmpty() ))
        {
            CreationClient.ajoutClientSession(session, newClient);
            commandeDAO.creer(newCommande);
            ajoutCommandeSession(session, newCommande);
            this.getServletContext().getRequestDispatcher( VUE ).forward( req, resp );
        }
        else
            this.getServletContext().getRequestDispatcher( FORM ).forward( req, resp );
    }

    public static void ajoutCommandeSession(HttpSession session, Commande newCommande)
    {
        List<Commande> listeCommandes = (List<Commande>)session.getAttribute(ATT_LISTE);
        if(listeCommandes == null)
        {
            listeCommandes = new ArrayList<Commande>();
            listeCommandes.add(newCommande);
            session.setAttribute(ATT_LISTE, listeCommandes);
        }
        else
        {
            listeCommandes.add(newCommande);
            session.setAttribute(ATT_LISTE, listeCommandes);
        }
    }
}
