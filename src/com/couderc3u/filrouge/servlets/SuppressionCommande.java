package com.couderc3u.filrouge.servlets;

import com.couderc3u.filrouge.dao.CommandeDAO;
import com.couderc3u.filrouge.entities.Commande;
import com.couderc3u.filrouge.dao.ClientDAO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@WebServlet( urlPatterns = { "/SuppressionCommande" } )
public class SuppressionCommande extends HttpServlet
{
    private static final String VUE = "/ListeCommandes";
    private static final String ATT_LISTE = "listeCommandes";
    private static final String ID_P = "id";

    public static final String CONF_DAO_FACTORY = "DAOfactory";

    @EJB
    private CommandeDAO commandeDAO;
    @EJB
    private ClientDAO clientDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        HttpSession session = req.getSession();
        Long id = Long.parseLong(req.getParameter(ID_P));
        remove(session, id);
        resp.sendRedirect( req.getContextPath() + VUE );
    }

    private void remove(HttpSession session, Long id)
    {
        Commande commande = commandeDAO.trouver(id);
        List<Commande> listeCommandes = (List<Commande>)session.getAttribute(ATT_LISTE);
        listeCommandes.removeIf(c -> c.getId().equals(id));
        commandeDAO.supprimer(commande);
        session.setAttribute(ATT_LISTE, listeCommandes);
    }
}