package com.couderc3u.filrouge.servlets;

import com.couderc3u.filrouge.entities.Client;
import com.couderc3u.filrouge.dao.ClientDAO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@WebServlet( urlPatterns = { "/SuppressionClient" } )
public class SuppressionClient extends HttpServlet
{
    private static final String VUE = "/ListeClients";
    private static final String ATT_LISTE = "listeClients";
    private static final String ID_P = "id";

    public static final String CONF_DAO_FACTORY = "DAOfactory";

    @EJB
    private ClientDAO clientDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        HttpSession session = req.getSession();
        Long id = Long.parseLong(req.getParameter(ID_P));
        remove(session, id);
        resp.sendRedirect( req.getContextPath() + VUE );
    }

    private void remove(HttpSession session, Long id)
    {
        Client client = clientDAO.trouver(id);
        List<Client> listeClients = (List<Client>)session.getAttribute(ATT_LISTE);
        listeClients.removeIf(c -> c.getId().equals(id));
        //listeClients.remove(client);
        clientDAO.supprimer(client);
        session.setAttribute(ATT_LISTE, listeClients);
    }
}